FROM registry.gitlab.com/drkloc/docker-python
MAINTAINER Teo Sibileau

# Update packages and install software
RUN npm install -g phantomjs-prebuilt
